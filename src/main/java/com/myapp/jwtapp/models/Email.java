package com.myapp.jwtapp.models;

public class Email {
	
	public String to;
	public String subject;
	public String messageText;
	public String attachment;
	
	public Email() {
		
	}

	public Email(String to, String subject, String messageText, String attachment) {
		this.to = to;
		this.subject = subject;
		this.messageText = messageText;
		this.attachment = attachment;
	}
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	
	
}
