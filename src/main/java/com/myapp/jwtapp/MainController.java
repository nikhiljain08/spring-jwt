package com.myapp.jwtapp;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.jwtapp.models.AuthenticationRequest;
import com.myapp.jwtapp.models.AuthenticationResponse;
import com.myapp.jwtapp.models.Email;
import com.myapp.jwtapp.services.EmailService;
import com.myapp.jwtapp.services.MyUserDetailsService;
import com.myapp.jwtapp.util.JwtUtil;

@RestController
public class MainController {

	@Autowired
	private AuthenticationManager authManager;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@RequestMapping({"/hello"})
	public String hello() {
		return "Hello World!"; 
	}
	
	@RequestMapping(value = "/sendmail", method = RequestMethod.POST)
    public String sendmail(@RequestBody Email email) throws MessagingException {
		//emailService.sendEmail(email);
        emailService.sendEmailWithAttachment(email);
        return "emailsent";
    }
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authRequest) throws Exception {
		try {
			authManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect Username or password!", e);
		}
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
		final String jwt = jwtUtil.generateToken(userDetails);
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
}
