package com.myapp.jwtapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class JwtappApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtappApplication.class, args);
	}

}
